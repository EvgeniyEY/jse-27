# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: EVGENIY ERMOLAEV

**E-MAIL**: ermolaev.evgeniy.96@yandex.ru

# SOFTWARE

- JDK 1.8

- Windows 10

# PROGRAM BUILD

```bash
mvn clean install
```

# PROGRAM RUN

```bash
java -jar ./taskDTO-manager.jar
```

# SCREENSHOTS

https://yadi.sk/d/53N8yoG_5j8LWg?w=1