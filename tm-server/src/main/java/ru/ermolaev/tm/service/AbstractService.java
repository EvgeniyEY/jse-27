package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.api.service.IService;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.entity.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final ServiceLocator serviceLocator;

    public AbstractService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
