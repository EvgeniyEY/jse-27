package ru.ermolaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;

public abstract class AbstractRepository<E extends AbstractEntity> {

    protected EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull final E e) {
        entityManager.persist(e);
    }

    public void merge(@NotNull final E e) {
        entityManager.merge(e);
    }

    public void remove(@NotNull final E e) {
        entityManager.remove(e);
    }

}
