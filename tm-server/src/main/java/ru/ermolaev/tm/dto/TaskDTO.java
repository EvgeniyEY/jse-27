package ru.ermolaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.entity.Task;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String userId;

    @Nullable
    private String projectId;

    @Nullable
    private Date startDate;

    @Nullable
    private Date completeDate;

    @Nullable
    private Date creationDate;

    @Override
    public String toString() {
        return "Task [" +
                "Name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ']';
    }

    @Nullable
    public static TaskDTO toDTO(@Nullable final Task task) {
        if (task == null) return null;
        return new TaskDTO(task);
    }

    @NotNull
    public static List<TaskDTO> toDTO(@Nullable final Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        for (@Nullable final Task task: tasks) {
            if (task == null) continue;
            result.add(new TaskDTO(task));
        }
        return result;
    }

    public TaskDTO(@Nullable final Task task) {
        if (task == null) return;
        id = task.getId();
        name = task.getName();
        description = task.getDescription();
        if (task.getUser() != null) userId = task.getUser().getId();
        if (task.getProject() != null) projectId = task.getProject().getId();
        if (task.getCreationDate() != null) creationDate = task.getCreationDate();
        if (task.getStartDate() != null) startDate = task.getStartDate();
        if (task.getCompleteDate() != null) completeDate = task.getCompleteDate();
    }

}
