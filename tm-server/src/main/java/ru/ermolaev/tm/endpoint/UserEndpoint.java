package ru.ermolaev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.endpoint.IUserEndpoint;
import ru.ermolaev.tm.api.service.ServiceLocator;
import ru.ermolaev.tm.dto.SessionDTO;
import ru.ermolaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public final class UserEndpoint implements IUserEndpoint {

    private ServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updatePassword(sessionDTO.getUserId(), newPassword);
    }

    @Override
    @WebMethod
    public void updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newFirstName", partName = "newFirstName") @Nullable final String newFirstName
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateUserFirstName(sessionDTO.getUserId(), newFirstName);
    }

    @Override
    @WebMethod
    public void updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newMiddleName", partName = "newMiddleName") @Nullable final String newMiddleName
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateUserMiddleName(sessionDTO.getUserId(), newMiddleName);
    }

    @Override
    @WebMethod
    public void updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newLastName", partName = "newLastName") @Nullable final String newLastName
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateUserLastName(sessionDTO.getUserId(), newLastName);
    }

    @Override
    @WebMethod
    public void updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "newEmail", partName = "newEmail") @Nullable final String newEmail
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateUserEmail(sessionDTO.getUserId(), newEmail);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findUserProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getSessionService().getUser(sessionDTO);
    }

}
