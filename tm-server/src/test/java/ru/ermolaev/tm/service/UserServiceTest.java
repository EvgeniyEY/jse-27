package ru.ermolaev.tm.service;

import org.junit.*;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.UserDTO;
import ru.ermolaev.tm.enumeration.Role;
import ru.ermolaev.tm.exception.empty.*;
import ru.ermolaev.tm.repository.UserRepository;
import ru.ermolaev.tm.util.HashUtil;

public class UserServiceTest {

    /*
        Требуется рефакторинг в связи с переходом на новую моель хранения данных
    */

//    final private IUserRepository userRepository = new UserRepository();
//
//    final private IUserService userService = new UserService(userRepository);
//
//    final private UserDTO userDTO = new UserDTO("test", "pass", "test.ru");
//
//    @Before
//    public void addUserToRepository() {
//        userRepository.add(userDTO);
//    }
//
//    @After
//    public void removeUserFromRepository() {
//        userRepository.remove(userDTO);
//    }
//
//    @Test
//    public void findByIdTest() throws Exception {
//        final UserDTO testUserDTO = userService.findById(userDTO.getId());
//        Assert.assertNotNull(testUserDTO);
//        Assert.assertEquals(userDTO.getId(), testUserDTO.getId());
//        Assert.assertNull(userService.findById("100"));
//    }
//
//    @Test
//    public void findByLoginTest() throws Exception {
//        final UserDTO testUserDTO = userService.findByLogin(userDTO.getLogin());
//        Assert.assertNotNull(testUserDTO);
//        Assert.assertEquals(userDTO.getId(), testUserDTO.getId());
//        Assert.assertNull(userService.findByLogin("admin"));
//    }
//
//    @Test
//    public void findByEmailTest() throws Exception {
//        final UserDTO testUserDTO = userService.findByEmail(userDTO.getEmail());
//        Assert.assertNotNull(testUserDTO);
//        Assert.assertEquals(userDTO.getId(), testUserDTO.getId());
//        Assert.assertNull(userService.findByEmail("email"));
//    }
//
//    @Test
//    public void removeUserTest() throws Exception {
//        Assert.assertNotNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(1, userService.findAll().size());
//        Assert.assertNull(userService.removeUser(null));
//        Assert.assertEquals(userDTO, userService.removeUser(userDTO));
//        Assert.assertNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(0, userService.findAll().size());
//    }
//
//    @Test
//    public void removeByIdTest() throws Exception {
//        Assert.assertNotNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(1, userService.findAll().size());
//        Assert.assertNull(userService.removeById("100"));
//        Assert.assertEquals(userDTO, userService.removeById(userDTO.getId()));
//        Assert.assertNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(0, userService.findAll().size());
//    }
//
//    @Test
//    public void removeByLoginTest() throws Exception {
//        Assert.assertNotNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(1, userService.findAll().size());
//        Assert.assertNull(userService.removeByLogin("100"));
//        Assert.assertEquals(userDTO, userService.removeByLogin(userDTO.getLogin()));
//        Assert.assertNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(0, userService.findAll().size());
//    }
//
//    @Test
//    public void removeByEmailTest() throws Exception {
//        Assert.assertNotNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(1, userService.findAll().size());
//        Assert.assertNull(userService.removeByEmail("100"));
//        Assert.assertEquals(userDTO, userService.removeByEmail(userDTO.getEmail()));
//        Assert.assertNull(userService.findById(userDTO.getId()));
//        Assert.assertEquals(0, userService.findAll().size());
//    }
//
//    @Test
//    public void createTest() throws Exception {
//        Assert.assertEquals(1, userService.findAll().size());
//        final UserDTO testUserDTO = userService.create("root", "pass");
//        Assert.assertNotNull(testUserDTO);
//        Assert.assertEquals(testUserDTO.getLogin(), userService.findById(testUserDTO.getId()).getLogin());
//        Assert.assertEquals(testUserDTO.getPasswordHash(), userService.findById(testUserDTO.getId()).getPasswordHash());
//        Assert.assertEquals(testUserDTO.getRole(), userService.findById(testUserDTO.getId()).getRole());
//        Assert.assertEquals(testUserDTO.getRole(), Role.USER);
//        Assert.assertEquals(2, userService.findAll().size());
//    }
//
//    @Test
//    public void createWithEmailTest() throws Exception {
//        Assert.assertEquals(1, userService.findAll().size());
//        final UserDTO testUserDTO = userService.create("root", "root", "email@test.ru");
//        Assert.assertNotNull(testUserDTO);
//        Assert.assertEquals(testUserDTO.getLogin(), userService.findById(testUserDTO.getId()).getLogin());
//        Assert.assertEquals(testUserDTO.getPasswordHash(), userService.findById(testUserDTO.getId()).getPasswordHash());
//        Assert.assertEquals(testUserDTO.getEmail(), userService.findById(testUserDTO.getId()).getEmail());
//        Assert.assertEquals(testUserDTO.getRole(), userService.findById(testUserDTO.getId()).getRole());
//        Assert.assertEquals(testUserDTO.getRole(), Role.USER);
//        Assert.assertEquals(2, userService.findAll().size());
//    }
//
//    @Test
//    public void createWithRoleTest() throws Exception {
//        Assert.assertEquals(1, userService.findAll().size());
//        final UserDTO testUserDTO = userService.create("root", "root", Role.ADMIN);
//        Assert.assertNotNull(testUserDTO);
//        Assert.assertEquals(testUserDTO.getLogin(), userService.findById(testUserDTO.getId()).getLogin());
//        Assert.assertEquals(testUserDTO.getRole(), userService.findById(testUserDTO.getId()).getRole());
//        Assert.assertEquals(testUserDTO.getRole(), Role.ADMIN);
//        Assert.assertEquals(2, userService.findAll().size());
//    }
//
//    @Test
//    public void updatePasswordTest() throws Exception {
//        Assert.assertEquals(userDTO.getPasswordHash(), userService.findById(userDTO.getId()).getPasswordHash());
//        Assert.assertNull(userService.updatePassword("id", "newPass"));
//        Assert.assertNotNull(userService.updatePassword(userDTO.getId(), "newPass"));
//        Assert.assertEquals(userDTO.getPasswordHash(), HashUtil.hidePassword("newPass"));
//    }
//
//    @Test
//    public void updateUserFirstNameTest() throws Exception {
//        Assert.assertEquals(userDTO.getFirstName(), userService.findById(userDTO.getId()).getFirstName());
//        Assert.assertNull(userService.updateUserFirstName("id", "newName"));
//        Assert.assertNotNull(userService.updateUserFirstName(userDTO.getId(), "newName"));
//        Assert.assertEquals("newName", userDTO.getFirstName());
//    }
//
//    @Test
//    public void updateUserMiddleNameTest() throws Exception {
//        Assert.assertEquals(userDTO.getMiddleName(), userService.findById(userDTO.getId()).getMiddleName());
//        Assert.assertNull(userService.updateUserMiddleName("id", "newName"));
//        Assert.assertNotNull(userService.updateUserMiddleName(userDTO.getId(), "newName"));
//        Assert.assertEquals("newName", userDTO.getMiddleName());
//    }
//
//    @Test
//    public void updateUserLastNameTest() throws Exception {
//        Assert.assertEquals(userDTO.getLastName(), userService.findById(userDTO.getId()).getLastName());
//        Assert.assertNull(userService.updateUserLastName("id", "newName"));
//        Assert.assertNotNull(userService.updateUserLastName(userDTO.getId(), "newName"));
//        Assert.assertEquals("newName", userDTO.getLastName());
//    }
//
//    @Test
//    public void updateUserEmailTest() throws Exception {
//        Assert.assertEquals(userDTO.getEmail(), userService.findById(userDTO.getId()).getEmail());
//        Assert.assertNull(userService.updateUserEmail("id", "newEmail"));
//        Assert.assertNotNull(userService.updateUserEmail(userDTO.getId(), "newEmail"));
//        Assert.assertEquals("newEmail", userDTO.getEmail());
//    }
//
//    @Test
//    public void lockUserByLoginTest() throws Exception {
//        Assert.assertNotNull(userService.findById(userDTO.getId()));
//        Assert.assertFalse(userDTO.getLocked());
//        Assert.assertNull(userService.lockUserByLogin("log"));
//        Assert.assertNotNull(userService.lockUserByLogin(userDTO.getLogin()));
//        Assert.assertTrue(userDTO.getLocked());
//    }
//
//    @Test
//    public void unlockUserByLoginTest() throws Exception {
//        Assert.assertNotNull(userService.lockUserByLogin(userDTO.getLogin()));
//        Assert.assertTrue(userDTO.getLocked());
//        Assert.assertNull(userService.unlockUserByLogin("log"));
//        Assert.assertNotNull(userService.unlockUserByLogin(userDTO.getLogin()));
//        Assert.assertFalse(userDTO.getLocked());
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void findByIdWithNullIdTest() throws Exception {
//        userService.findById(null);
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void findByIdWithEmptyIdTest() throws Exception {
//        userService.findById("");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void findByLoginWithNullLoginTest() throws Exception {
//        userService.findByLogin(null);
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void findByLoginWithEmptyLoginTest() throws Exception {
//        userService.findByLogin("");
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void findByEmailWithNullEmailTest() throws Exception {
//        userService.findByEmail(null);
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void findByEmailWithEmptyEmailTest() throws Exception {
//        userService.findByEmail("");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void removeByIdWithNullIdTest() throws Exception {
//        userService.removeById(null);
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void removeByIdWithEmptyIdTest() throws Exception {
//        userService.removeById("");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void removeByLoginWithNullLoginTest() throws Exception {
//        userService.removeByLogin(null);
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void removeByLoginWithEmptyLoginTest() throws Exception {
//        userService.removeByLogin("");
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void removeByEmailWithNullEmailTest() throws Exception {
//        userService.removeByEmail(null);
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void removeByEmailWithEmptyEmailTest() throws Exception {
//        userService.removeByEmail("");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void creatWithNullLoginTest() throws Exception {
//        userService.create(null,"pass");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void creatWithEmptyLoginTest() throws Exception {
//        userService.create("","pass");
//    }
//
//    @Test(expected = EmptyPasswordException.class)
//    public void creatWithNullPasswordTest() throws Exception {
//        userService.create("test",null);
//    }
//
//    @Test(expected = EmptyPasswordException.class)
//    public void creatWithEmptyPasswordTest() throws Exception {
//        userService.create("test","");
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void creatWithNullEmailTest() throws Exception {
//        String pass = null;
//        userService.create("test","pass", pass);
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void creatWithEmptyEmailTest() throws Exception {
//        userService.create("test","pass", "");
//    }
//
//    @Test(expected = EmptyRoleException.class)
//    public void creatWithNullRoleTest() throws Exception {
//        Role role = null;
//        userService.create("test","pass", role);
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updatePasswordWithNullLoginTest() throws Exception {
//        userService.updatePassword(null,"pass");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updatePasswordWithEmptyLoginTest() throws Exception {
//        userService.updatePassword("","pass");
//    }
//
//    @Test(expected = EmptyPasswordException.class)
//    public void updatePasswordWithNullPasswordTest() throws Exception {
//        userService.updatePassword("test",null);
//    }
//
//    @Test(expected = EmptyPasswordException.class)
//    public void updatePasswordWithEmptyPasswordTest() throws Exception {
//        userService.updatePassword("test","");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserFirstNameWithNullUserIdTest() throws Exception {
//        userService.updateUserFirstName(null,"pass");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserFirstNameWithEmptyUserIdTest() throws Exception {
//        userService.updateUserFirstName("","pass");
//    }
//
//    @Test(expected = EmptyFirstNameException.class)
//    public void updateUserFirstNameWithNullNameTest() throws Exception {
//        userService.updateUserFirstName("test",null);
//    }
//
//    @Test(expected = EmptyFirstNameException.class)
//    public void updateUserFirstNameWithEmptyNameTest() throws Exception {
//        userService.updateUserFirstName("test","");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserMiddleNameWithNullUserIdTest() throws Exception {
//        userService.updateUserMiddleName(null,"pass");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserMiddleNameWithEmptyUserIdTest() throws Exception {
//        userService.updateUserMiddleName("","pass");
//    }
//
//    @Test(expected = EmptyMiddleNameException.class)
//    public void updateUserMiddleNameWithNullNameTest() throws Exception {
//        userService.updateUserMiddleName("test",null);
//    }
//
//    @Test(expected = EmptyMiddleNameException.class)
//    public void updateUserMiddleNameWithEmptyNameTest() throws Exception {
//        userService.updateUserMiddleName("test","");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserLastNameWithNullUserIdTest() throws Exception {
//        userService.updateUserLastName(null,"pass");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserLastNameWithEmptyUserIdTest() throws Exception {
//        userService.updateUserLastName("","pass");
//    }
//
//    @Test(expected = EmptyLastNameException.class)
//    public void updateUserLastNameWithNullNameTest() throws Exception {
//        userService.updateUserLastName("test",null);
//    }
//
//    @Test(expected = EmptyLastNameException.class)
//    public void updateUserLastNameWithEmptyNameTest() throws Exception {
//        userService.updateUserLastName("test","");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserEmailWithNullUserIdTest() throws Exception {
//        userService.updateUserEmail(null,"pass");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void updateUserEmailWithEmptyUserIdTest() throws Exception {
//        userService.updateUserEmail("","pass");
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void updateUserEmailWithNullEmailTest() throws Exception {
//        userService.updateUserEmail("test",null);
//    }
//
//    @Test(expected = EmptyEmailException.class)
//    public void updateUserEmailWithEmptyEmailTest() throws Exception {
//        userService.updateUserEmail("test","");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void lockUserByLoginWithNullUserIdTest() throws Exception {
//        userService.lockUserByLogin(null);
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void lockUserByLoginWithEmptyUserIdTest() throws Exception {
//        userService.lockUserByLogin("");
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void unlockUserByLoginWithNullUserIdTest() throws Exception {
//        userService.unlockUserByLogin(null);
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void unlockUserByLoginWithEmptyUserIdTest() throws Exception {
//        userService.unlockUserByLogin("");
//    }

}
