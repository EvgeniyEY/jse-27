package ru.ermolaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.ermolaev.tm.command.AbstractCommand;

import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (@NotNull final AbstractCommand command: commands) {
            System.out.println(command.commandName()
                    + (command.arg() == null ? ": " : ", " + command.arg() + ": ")
                    + command.description());
        }
    }

}
