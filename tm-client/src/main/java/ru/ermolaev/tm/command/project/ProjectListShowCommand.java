package ru.ermolaev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.ProjectDTO;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public final class ProjectListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECTS LIST]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        @NotNull final List<ProjectDTO> projects = serviceLocator.getProjectEndpoint().showAllProjects(session);
        if (projects == null) return;
        for (@NotNull final ProjectDTO project: projects) {
            System.out.println((projects.indexOf(project) + 1)
                    + ". {id: "
                    + project.getId()
                    + "; name: "
                    + project.getName()
                    + "; description: "
                    + project.getDescription()
                    + "}");
        }
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
