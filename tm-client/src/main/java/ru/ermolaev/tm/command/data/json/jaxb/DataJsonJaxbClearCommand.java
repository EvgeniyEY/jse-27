package ru.ermolaev.tm.command.data.json.jaxb;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.enumeration.Role;

public final class DataJsonJaxbClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "data-json-jb-clear";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove json (Jax-B) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE JSON (JAX-B) FILE]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        serviceLocator.getAdminDataEndpoint().clearJsonFileJaxb(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
