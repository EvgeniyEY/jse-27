package ru.ermolaev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.command.AbstractCommand;
import ru.ermolaev.tm.endpoint.SessionDTO;
import ru.ermolaev.tm.endpoint.TaskDTO;
import ru.ermolaev.tm.enumeration.Role;

import java.util.List;

public final class TaskListShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandName() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getCurrentSession();
        @Nullable final List<TaskDTO> tasks = serviceLocator.getTaskEndpoint().showAllTasks(session);
        if (tasks == null) return;
        for (@NotNull final TaskDTO task: tasks) {
            System.out.println((tasks.indexOf(task) + 1)
                    + ". {id: "
                    + task.getId()
                    + "; name: "
                    + task.getName()
                    + "; description: "
                    + task.getDescription()
                    + "}");
        }
        System.out.println("[COMPLETE]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.USER, Role.ADMIN };
    }

}
