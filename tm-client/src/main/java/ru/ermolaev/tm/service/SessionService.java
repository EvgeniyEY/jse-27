package ru.ermolaev.tm.service;

import org.jetbrains.annotations.Nullable;
import ru.ermolaev.tm.api.ISessionService;
import ru.ermolaev.tm.endpoint.SessionDTO;

public final class SessionService implements ISessionService {

    @Nullable
    private SessionDTO currentSession;

    @Nullable
    @Override
    public SessionDTO getCurrentSession() {
        return currentSession;
    }

    @Override
    public void setCurrentSession(SessionDTO currentSession) {
        this.currentSession = currentSession;
    }

    @Override
    public void clearCurrentSession() {
        currentSession = null;
    }

}
