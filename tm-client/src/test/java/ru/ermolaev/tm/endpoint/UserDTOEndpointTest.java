package ru.ermolaev.tm.endpoint;

import com.sun.xml.internal.ws.fault.*;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import org.junit.Assert;
import ru.ermolaev.tm.bootstrap.Bootstrap;

public class UserDTOEndpointTest {

    private final Bootstrap bootstrap = new Bootstrap();

    private final String testLogin = "userForTest";

    private final String testPass = "testPass";

    private final String testEmail = "testuser@test.ru";

    private final String testFirstName = "Ivan";

    private final String testMiddleName = "Ivanovich";

    private final String testLastName = "Ivanov";

    @Before
    public void prepareToTest() throws java.lang.Exception {
        bootstrap.getAdminUserEndpoint().createUserWithEmail(testLogin, testPass, testEmail);
        final SessionDTO session = bootstrap.getSessionEndpoint().openSession(testLogin, testPass);
        bootstrap.getSessionService().setCurrentSession(session);
    }

    @After
    public void clearAfterTest() throws java.lang.Exception {
        final SessionDTO adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        final SessionDTO testUserSession = bootstrap.getSessionService().getCurrentSession();
        bootstrap.getSessionEndpoint().closeSession(testUserSession);
        bootstrap.getSessionService().clearCurrentSession();
        bootstrap.getAdminUserEndpoint().removeUserByLogin(adminSession,testLogin);
        bootstrap.getSessionEndpoint().closeSession(adminSession);
    }

    @Test
    public void updateUserPasswordTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final String oldPass = bootstrap.getUserEndpoint().showUserProfile(session).getPasswordHash();
        Assert.assertNotNull(oldPass);
        Assert.assertFalse(oldPass.isEmpty());
        bootstrap.getUserEndpoint().updateUserPassword(session, "newPass");
        final String newPass = bootstrap.getUserEndpoint().showUserProfile(session).getPasswordHash();
        Assert.assertNotNull(newPass);
        Assert.assertFalse(newPass.isEmpty());
        Assert.assertNotEquals(oldPass, newPass);
    }

    @Test
    public void updateUserEmailTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final String oldEmail = bootstrap.getUserEndpoint().showUserProfile(session).getEmail();
        Assert.assertNotNull(oldEmail);
        Assert.assertEquals(testEmail, oldEmail);
        bootstrap.getUserEndpoint().updateUserEmail(session, "newEmail");
        final String newEmail = bootstrap.getUserEndpoint().showUserProfile(session).getEmail();
        Assert.assertNotNull(newEmail);
        Assert.assertEquals("newEmail", newEmail);
        Assert.assertNotEquals(oldEmail, newEmail);
    }

    @Test
    public void updateUserFirstNameTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final String oldFirstName = bootstrap.getUserEndpoint().showUserProfile(session).getFirstName();
        Assert.assertNotNull(oldFirstName);
        bootstrap.getUserEndpoint().updateUserFirstName(session, testFirstName);
        final String newFirstName = bootstrap.getUserEndpoint().showUserProfile(session).getFirstName();
        Assert.assertNotNull(newFirstName);
        Assert.assertEquals(testFirstName, newFirstName);
        Assert.assertNotEquals(oldFirstName, newFirstName);
    }

    @Test
    public void updateUserMiddleNameTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final String oldMiddleName = bootstrap.getUserEndpoint().showUserProfile(session).getMiddleName();
        Assert.assertNotNull(oldMiddleName);
        bootstrap.getUserEndpoint().updateUserMiddleName(session, testMiddleName);
        final String newMiddleName = bootstrap.getUserEndpoint().showUserProfile(session).getMiddleName();
        Assert.assertNotNull(newMiddleName);
        Assert.assertEquals(testMiddleName, newMiddleName);
        Assert.assertNotEquals(oldMiddleName, newMiddleName);
    }

    @Test
    public void updateUserLastNameTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        final String oldLastName = bootstrap.getUserEndpoint().showUserProfile(session).getLastName();
        Assert.assertNotNull(oldLastName);
        bootstrap.getUserEndpoint().updateUserLastName(session, testLastName);
        final String newLastName = bootstrap.getUserEndpoint().showUserProfile(session).getLastName();
        Assert.assertNotNull(newLastName);
        Assert.assertEquals(testLastName, newLastName);
        Assert.assertNotEquals(oldLastName, newLastName);
    }

    @Test
    public void showProfileTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        Assert.assertNotNull(session);
        bootstrap.getUserEndpoint().updateUserFirstName(session, testFirstName);
        bootstrap.getUserEndpoint().updateUserMiddleName(session, testMiddleName);
        bootstrap.getUserEndpoint().updateUserLastName(session, testLastName);
        final UserDTO user = bootstrap.getUserEndpoint().showUserProfile(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(testLogin, user.getLogin());
        Assert.assertEquals(testEmail, user.getEmail());
        Assert.assertEquals(testFirstName, user.getFirstName());
        Assert.assertEquals(testMiddleName, user.getMiddleName());
        Assert.assertEquals(testLastName, user.getLastName());
        Assert.assertEquals(Role.USER, user.getRole());
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserPasswordWithNullSessionTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().updateUserPassword(null, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserPasswordWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getUserEndpoint().updateUserPassword(fakeSession, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserFirstNameWithNullSessionTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().updateUserFirstName(null, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserFirstNameWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getUserEndpoint().updateUserFirstName(fakeSession, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserMiddleNameWithNullSessionTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().updateUserMiddleName(null, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserMiddleNameWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getUserEndpoint().updateUserMiddleName(fakeSession, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserLastNameWithNullSessionTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().updateUserLastName(null, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserLastNameWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getUserEndpoint().updateUserLastName(fakeSession, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserEmailWithNullSessionTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().updateUserEmail(null, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void updateUserEmailWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getUserEndpoint().updateUserEmail(fakeSession, "new");
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showUserProfileWithNullSessionTest() throws Exception_Exception {
        bootstrap.getUserEndpoint().showUserProfile(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void showUserProfileWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getUserEndpoint().showUserProfile(fakeSession);
    }

}
