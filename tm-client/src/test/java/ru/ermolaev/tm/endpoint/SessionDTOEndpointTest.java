package ru.ermolaev.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.ermolaev.tm.bootstrap.Bootstrap;

public class SessionDTOEndpointTest {

    private final Bootstrap bootstrap = new Bootstrap();

    @Test
    public void openCloseSessionTest() throws Exception_Exception {
        SessionDTO session = bootstrap.getSessionEndpoint().openSession("admin", "");
        Assert.assertNull(session);
        session = bootstrap.getSessionEndpoint().openSession("admin", null);
        Assert.assertNull(session);
        session = bootstrap.getSessionEndpoint().openSession("", "admin");
        Assert.assertNull(session);
        session = bootstrap.getSessionEndpoint().openSession(null, "admin");
        Assert.assertNull(session);
        session = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        Assert.assertNotNull(session);
        Assert.assertEquals(bootstrap.getUserEndpoint().showUserProfile(session).getId(), session.userId);
        Assert.assertNotNull(bootstrap.getSessionEndpoint().closeSession(session));
    }

    @Test
    public void closeAllSessions() throws Exception_Exception {
        SessionDTO session = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        for (int i = 0; i < 5; i++) {
            bootstrap.getSessionEndpoint().openSession("admin", "admin");
        }
        Assert.assertNotNull(session);
        Assert.assertNotNull(bootstrap.getSessionEndpoint().closeAllSessionsForUser(session));
    }

}
