package ru.ermolaev.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.Test;
import org.junit.After;
import org.junit.Before;
import ru.ermolaev.tm.bootstrap.Bootstrap;

public class AdminDataEndpointTest {

    private final Bootstrap bootstrap = new Bootstrap();

    @Before
    public void prepareToTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionEndpoint().openSession("admin", "admin");
        bootstrap.getSessionService().setCurrentSession(session);
    }
    
    @After
    public void clearAfterTest() throws Exception_Exception {
        final SessionDTO session = bootstrap.getSessionService().getCurrentSession();
        bootstrap.getSessionEndpoint().closeSession(session);
        bootstrap.getSessionService().clearCurrentSession();
    }
    
    @Test(expected = ServerSOAPFaultException.class)
    public void saveXmlByJaxbWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().saveXmlByJaxb(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveXmlByJaxbWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().saveXmlByJaxb(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadXmlByJaxbWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().loadXmlByJaxb(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadXmlByJaxbWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().loadXmlByJaxb(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearXmlFileJaxbWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().clearXmlFileJaxb(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearXmlFileJaxbWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().clearXmlFileJaxb(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveXmlByFasterXmlWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().saveXmlByFasterXml(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveXmlByFasterXmlWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().saveXmlByFasterXml(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadXmlByFasterXmlWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().loadXmlByFasterXml(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadXmlByFasterXmlWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().loadXmlByFasterXml(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearXmlFileFasterXmlWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().clearXmlFileFasterXml(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearXmlFileFasterXmlWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().clearXmlFileFasterXml(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveJsonByJaxbWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().saveJsonByJaxb(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveJsonByJaxbWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().saveJsonByJaxb(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadJsonByJaxbWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().loadJsonByJaxb(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadJsonByJaxbWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().loadJsonByJaxb(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearJsonFileJaxbWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().clearJsonFileJaxb(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearJsonFileJaxbWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().clearJsonFileJaxb(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveJsonByFasterXmlWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().saveJsonByFasterXml(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveJsonByFasterXmlWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().saveJsonByFasterXml(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadJsonByFasterXmlWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().loadJsonByFasterXml(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadJsonByFasterXmlWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().loadJsonByFasterXml(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearJsonFileFasterXmlWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().clearJsonFileFasterXml(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearJsonFileFasterXmlWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().clearJsonFileFasterXml(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveBinaryWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().saveBinary(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveBinaryWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().saveBinary(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadBinaryWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().loadBinary(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadBinaryWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().loadBinary(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearBinaryWithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().clearBinaryFile(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearBinaryWithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().clearBinaryFile(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveBase64WithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().saveBase64(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void saveBase64WithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().saveBase64(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadBase64WithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().loadBase64(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void loadBase64WithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().loadBase64(fakeSession);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearBase64WithNullSessionTest() throws Exception_Exception {
        bootstrap.getAdminDataEndpoint().clearBase64File(null);
    }

    @Test(expected = ServerSOAPFaultException.class)
    public void clearBase64WithInvalidSessionTest() throws Exception_Exception {
        final SessionDTO fakeSession = new SessionDTO();
        bootstrap.getAdminDataEndpoint().clearBase64File(fakeSession);
    }

}
